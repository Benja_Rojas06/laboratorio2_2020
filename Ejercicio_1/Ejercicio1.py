# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import json
import csv
import os


# Función para cargar un archivo.
def load_file():

    with open("top50.csv") as csv_file:
        canciones = list(csv.reader(csv_file))

    return canciones


# Función para guardar modificaciones hechas al archivo.
def save_file(diccionario):

    with open("minitop.json", "w") as file:
        json.dump(diccionario, file)


# Función que realiza las comparaciones de la función
# "contador_canciones".
def mas_canciones(cantidad):

    aux1 = 0
    aux2 = 0

    for i in range(len(cantidad)):

        if(i != 0):
            if(cantidad[i] > aux1):
                aux1 = cantidad[i]
                aux2 = i

    return aux2


# Función encargada de determinar que artista tiene
# más canciones en el TOP mediante comparaciones.
def contador_canciones(nombres_artistas, canciones):

    cantidad = []

    for i in range(len(nombres_artistas)):
        contador = 0
        for j in range(len(canciones)):
            if nombres_artistas[i] == canciones[j][2]:
                contador = contador + 1

        cantidad.append(contador)
        contador_canciones = mas_canciones(cantidad)

    return contador_canciones


# Función encargada de comprobar la existencia de un
# artista para que este solo se agregue una vez.
def comparador(nombres_artistas, artista):

    for i in range(len(nombres_artistas)):
        if(artista == nombres_artistas[i]):
            return True

    return False


# Función que retorna la columna que se quiera trabajar.
def columna_a_usar(canciones, j):

    columna = []

    for i in range(len(canciones)):
        columna.append(canciones[i][j])

    return columna


# Función que recorre y ordena la columna para así poder
# trabajar con todos sus datos.
def recorrer_columna(columna_a_recorrer):

    for i in range(len(columna_a_recorrer)):
        if(i != 0):
            temporal = int(columna_a_recorrer[i])
            columna_a_recorrer[i] = temporal

    columna_a_recorrer[0] = -60

    #Método que ordena de los datos.
    columna_a_recorrer.sort()

    return columna_a_recorrer


# Función que recorre una fila determinada para
# obtener todos sus datos.
def recorrer_filas(fila_a_recorrer):

    canciones = load_file()
    aux = 0

    fila = fila_a_recorrer[len(fila_a_recorrer) - 1]
    fila_a_recorrer = columna_a_usar(canciones, 13)

    for i in range(len(fila_a_recorrer)):
        if(i != 0):
            if(fila == int(fila_a_recorrer[i])):
                aux = i

    return aux


# Función que indica los artistas que apareden en el TOP
# (Sin repetirse), la cantidad que aparecen y el que posee
# más canciones.
def artistas():

    canciones = load_file()
    nombres_artistas = []

    for i in range(len(canciones)):

        # No se toma en cuenta el título de la columna.
        if(i == 0):
            pass

        else:
            comparacion = comparador(nombres_artistas, canciones[i][2])

            # Validación de que el artista solo aparece una vez.
            if(comparacion == False):
                nombres_artistas.append(canciones[i][2])

    print("\n")
    for i in range(len(nombres_artistas)):
        print(nombres_artistas[i])

    print("\n")
    # Se le resta el título.
    print("La cantidad de artistas que aparecen en el top es: "
          , (len(nombres_artistas) - 1))

    cant_canciones = contador_canciones(nombres_artistas, canciones)
    print("El artista que tiene más canciones es: "
          , nombres_artistas[cant_canciones])


# Función encargada de obtener la mediana de la columna de ruido.
def mediana_de_ruido():

    canciones = load_file()
    columna_ruido = columna_a_usar(canciones, 7)
    ruido = recorrer_columna(columna_ruido)

    # Cálculo matemático.
    mediana = int(len(ruido)/2)
    print("La mediana de ruido es: ", ruido[mediana], "y son dos canciones las"
          " que tienen este valor")


# Función que responde a la pregunta:
# ¿Es la canción más popular una de las menos ruidosas?
def pregunta():

    canciones = load_file()
    # Se obtienen las columnas que se van a utilizar
    columna_popularidad = columna_a_usar(canciones, 13)
    columna_ruido = columna_a_usar(canciones, 7)
    columna_canciones = columna_a_usar(canciones, 1)

    # Recorre y ordena los datos de la columna de popularidad para
    # trabajar con el mayor de ellos.
    popularidad = recorrer_columna(columna_popularidad)
    # Se obtiene la fila con la que se trabajará (La más popular).
    popular = recorrer_filas(popularidad)

    print("La canción más popular del TOP es: ", canciones[popular])

    ruido = recorrer_columna(columna_ruido)
    ruidosa = recorrer_filas(ruido)

    # Comprobaciones de lo solicitado.
    if(popular != ruidosa):
        print("Pero NO es la más ruidosa")

    else:
        columna_artista = columna_a_usar(canciones, 2)
        print("La canción más ruidosa es de: ", columna_artista[popular])
        print("Su popularidad es: ", columna_popularidad[popular])
        print("Su ruido es de: ", columna_ruido[popular])


# Función que crea el archivo "json".
def crear_diccionario():

    # Se crea un diccionario vacío para luego ser rellenado.
    diccionario = {}
    save_file(diccionario)

    canciones = load_file()

    # Se recopilan las columnas solicitadas en el problema.
    columna_ranking = columna_a_usar(canciones, 0)
    columna_canciones = columna_a_usar(canciones, 1)
    columna_artista = columna_a_usar(canciones, 2)
    columna_genero = columna_a_usar(canciones, 3)
    columna_tempo = columna_a_usar(canciones, 4)
    columna_duracion = columna_a_usar(canciones, 10)
    columna_popularidad = columna_a_usar(canciones, 13)

    # Se recorre ranking ya que dentro de este iŕan
    # todas las categorías.
    for i in range(len(columna_ranking)):

        # Se salta los títulos de las columnas.
        if(i == 0):
            pass

        # Asigna por ranking los valores respectivos.
        else:
            diccionario[columna_ranking[i]] = (columna_canciones[i]
                                              , columna_artista[i]
                                              , columna_genero[i]
                                              , columna_tempo[i]
                                              , columna_duracion[i]
                                              , columna_popularidad[i])

    save_file(diccionario)
    print("Revisar el archivo 'minitop.json'")


# Función menú con todas las acciones que tiene el programa.
def menu():

    while True:
        print("\n")
        print("¡¡¡Usted está a punto de ver el TOP 50 de Spotify!!!")
        print("¿Qué acción desea realizar?")
        print("\n")
        print("A/a: Para visualizar la cantidad de artistas y ver que artista"
              " tiene más canciones")
        print("M/m: Para calcular la mediana del ruido de las canciones")
        print("G/g: Para visualizar los tres géneros más bailables y las tres"
              " canciones más lentas")
        print("P/p: ¿Es la canción más popular una de las menos ruidosas?")
        print("J/j: Para generar un archivo json con las canciones")
        print("S/s: Para salir")

        print("\n")
        accion = input("Ingrese la acción a realizar: ")

        if(accion.upper() == "A"):
            artistas()

        elif(accion.upper() == "M"):
            mediana_de_ruido()

        elif(accion.upper() == "G"):
            print("Todavía no se encuentra disponible esta acción")

        elif(accion.upper() == "P"):
            pregunta()

        elif(accion.upper() == "J"):
            crear_diccionario()

        elif(accion.upper() == "S"):
            quit()

        else:
            os.system("clear")
            print("La opción que ha ingresado no es válida, seleccionela"
                  " nuevamente por favor")
            print("\n")
            pass


if __name__ == '__main__':

    menu()
